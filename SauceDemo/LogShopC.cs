﻿///////////////////////////////////////////////////////////////////////////////
//
// This file was automatically generated by RANOREX.
// DO NOT MODIFY THIS FILE! It is regenerated by the designer.
// All your modifications will be lost!
// http://www.ranorex.com
//
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;

using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Testing;
using Ranorex.Core.Repository;

namespace SauceDemo
{
#pragma warning disable 0436 //(CS0436) The type 'type' in 'assembly' conflicts with the imported type 'type2' in 'assembly'. Using the type defined in 'assembly'.
    /// <summary>
    ///The LogShopC recording.
    /// </summary>
    [TestModule("e2857c54-2ac5-4c26-b7a4-9e403e3e4387", ModuleType.Recording, 1)]
    public partial class LogShopC : ITestModule
    {
        /// <summary>
        /// Holds an instance of the SauceDemoRepository repository.
        /// </summary>
        public static SauceDemoRepository repo = SauceDemoRepository.Instance;

        static LogShopC instance = new LogShopC();

        /// <summary>
        /// Constructs a new instance.
        /// </summary>
        public LogShopC()
        {
        }

        /// <summary>
        /// Gets a static instance of this recording.
        /// </summary>
        public static LogShopC Instance
        {
            get { return instance; }
        }

#region Variables

#endregion

        /// <summary>
        /// Starts the replay of the static recording <see cref="Instance"/>.
        /// </summary>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", global::Ranorex.Core.Constants.CodeGenVersion)]
        public static void Start()
        {
            TestModuleRunner.Run(Instance);
        }

        /// <summary>
        /// Performs the playback of actions in this recording.
        /// </summary>
        /// <remarks>You should not call this method directly, instead pass the module
        /// instance to the <see cref="TestModuleRunner.Run(ITestModule)"/> method
        /// that will in turn invoke this method.</remarks>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", global::Ranorex.Core.Constants.CodeGenVersion)]
        void ITestModule.Run()
        {
            Mouse.DefaultMoveTime = 300;
            Keyboard.DefaultKeyPressTime = 20;
            Delay.SpeedFactor = 1.00;

            Init();

            Report.Log(ReportLevel.Info, "Keyboard", "Key sequence 'standard_user' with focus on 'ApplicationUnderTest.UserName'.", repo.ApplicationUnderTest.UserNameInfo, new RecordItemIndex(0));
            repo.ApplicationUnderTest.UserName.PressKeys("standard_user");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Keyboard", "Key sequence 'secret_sauce' with focus on 'ApplicationUnderTest.Password'.", repo.ApplicationUnderTest.PasswordInfo, new RecordItemIndex(1));
            repo.ApplicationUnderTest.Password.PressKeys("secret_sauce");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Invoke action", "Invoking PerformClick() on item 'ApplicationUnderTest.LoginButton'.", repo.ApplicationUnderTest.LoginButtonInfo, new RecordItemIndex(2));
            repo.ApplicationUnderTest.LoginButton.PerformClick();
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Invoke action", "Invoking PerformClick() on item 'ApplicationUnderTest.AddToCartSauceLabsBackpack'.", repo.ApplicationUnderTest.AddToCartSauceLabsBackpackInfo, new RecordItemIndex(3));
            repo.ApplicationUnderTest.AddToCartSauceLabsBackpack.PerformClick();
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Validation", "Validating AttributeEqual (InnerText='1') on item 'ApplicationUnderTest.Number_product'.", repo.ApplicationUnderTest.Number_productInfo, new RecordItemIndex(4));
            Validate.AttributeEqual(repo.ApplicationUnderTest.Number_productInfo, "InnerText", "1");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Invoke action", "Invoking PerformClick() on item 'ApplicationUnderTest.ShoppingCartLink'.", repo.ApplicationUnderTest.ShoppingCartLinkInfo, new RecordItemIndex(5));
            repo.ApplicationUnderTest.ShoppingCartLink.PerformClick();
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Validation", "Validating AttributeEqual (InnerText='Sauce Labs Backpack') on item 'ApplicationUnderTest.SauceLabsBackpack'.", repo.ApplicationUnderTest.SauceLabsBackpackInfo, new RecordItemIndex(6));
            Validate.AttributeEqual(repo.ApplicationUnderTest.SauceLabsBackpackInfo, "InnerText", "Sauce Labs Backpack");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Invoke action", "Invoking PerformClick() on item 'ApplicationUnderTest.Checkout'.", repo.ApplicationUnderTest.CheckoutInfo, new RecordItemIndex(7));
            repo.ApplicationUnderTest.Checkout.PerformClick();
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Invoke action", "Invoking PerformClick() on item 'ApplicationUnderTest.Cancel'.", repo.ApplicationUnderTest.CancelInfo, new RecordItemIndex(8));
            repo.ApplicationUnderTest.Cancel.PerformClick();
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Invoke action", "Invoking PerformClick() on item 'ApplicationUnderTest.RemoveSauceLabsBackpack'.", repo.ApplicationUnderTest.RemoveSauceLabsBackpackInfo, new RecordItemIndex(9));
            repo.ApplicationUnderTest.RemoveSauceLabsBackpack.PerformClick();
            Delay.Milliseconds(0);
            
        }

#region Image Feature Data
#endregion
    }
#pragma warning restore 0436
}
